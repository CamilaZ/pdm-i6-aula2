package br.edu.ifsc.meuapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageButton malt = (ImageButton) findViewById(R.id.imageButton);
        ImageButton bock = (ImageButton) findViewById(R.id.imageButton2);
        ImageButton lagerr = (ImageButton) findViewById(R.id.imageButton3);
        malt.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent bas = new Intent(MainActivity.this, Malt.class);
                startActivity(bas);
            }
        });
        bock.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent bas = new Intent(MainActivity.this, Bock.class);
                startActivity(bas);
            }
        });
        lagerr.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent bas = new Intent(MainActivity.this, lager.class);
                startActivity(bas);
            }
        });
    }
}
