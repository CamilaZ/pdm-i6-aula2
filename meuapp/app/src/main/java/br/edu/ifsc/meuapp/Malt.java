package br.edu.ifsc.meuapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Malt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_malt);
        Button volta = (Button) findViewById(R.id.vlt2);
        volta.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent bas = new Intent(Malt.this, MainActivity.class);
                startActivity(bas);
            }
        });
    }
}
